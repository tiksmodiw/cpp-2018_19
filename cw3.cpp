#include <iostream>
#include <math.h>

using namespace std;

class Zespolona
{
    double a,b;

public:
    Zespolona()
    {
        a=b=0;
    }
    double modul()
    {
        return sqrt(a*a+b*b);
    }
    Zespolona operator+ ( Zespolona &q)
    {
        Zespolona temp;
        temp.a = a + q.a;
        temp.b = b + q.b;
        return temp;
    }
    Zespolona operator- ( Zespolona &q)
    {
        Zespolona temp;
        temp.a = a - q.a;
        temp.b = b - q.b;
        return temp;
    }
    Zespolona operator* ( Zespolona &q)
    {
        Zespolona temp;
        temp.a = (a * q.a)-(b *q.b);
        temp.b = (a*q.b)+(q.a*b);
        return temp;
    }
    Zespolona operator/ ( Zespolona &q)
    {
        Zespolona temp;
        temp.a = (((a*q.a)+b*q.b)/(q.a*q.a+q.b*q.b));
        temp.b = ((b*q.a)-(a*q.b)/(q.a*q.a+q.b*q.b));
        return temp;
    }
    bool operator< (Zespolona &q)
    {
        Zespolona temp;
        temp.a = a - q.a;
        temp.b = b - q.b;
        if(temp.a < 0 || temp.b < 0)
            return false;
        else
            return true;

    }
    bool operator== ( Zespolona &q)
    {
        if((a == q.a) &&(b == q.b))
            return true;
        else
            return false;
    }
    bool rozneOdZera()
    {
        if( a != 0 && b != 0)
        {
            return true;
        }
        else
            return false;
    }


    friend ostream & operator<< (ostream &wyjscie, const Zespolona &s);
    friend istream & operator>> (istream &wejscie, Zespolona &s);
    friend double katMiedzyWektorami(Zespolona &p1, Zespolona &p2);
};


ostream & operator<< (ostream &wyjscie, const Zespolona &s)
{
    return wyjscie << s.a <<" + " << s.b <<  "i "<< endl;
}

istream & operator>> (istream &wejscie, Zespolona &s)
{
    wejscie >> s.a;
    wejscie >> s.b;
    return wejscie;
}


// <<<------------------------------- CW3 ------------------------------- >>>

double katMiedzyWektorami(Zespolona &p1, Zespolona &p2)
{
    double dlugosc1, dlugosc2, iloskalarny,cosalfa;
    iloskalarny = p1.a*p2.a+p1.b*p2.b;
    dlugosc1 = p1.modul();
    dlugosc2 = p2.modul();

    cosalfa = ((iloskalarny)/(dlugosc1*dlugosc2));
    return cos(cosalfa);
}

void obslugaMenu()
{
    cout << "wybierz cwiczenie" << endl;
    cout << "1. CW 1" << endl;
    cout << "2. CW 2" << endl;
    cout << "3. CW 3" << endl;
    cout << "9. wyjscie z programu" << endl;
}
void obslugaCW2()
{
    cout << "wybierz opcje:" << endl;
    cout << "1. wczytaj dwie liczby zespolone z klawiatury" << endl;
    cout << "2. Dodaj dwie liczby zespolone i wypisz wynik" << endl;
    cout << "3. Odejmij pierwsza liczbe od drugiej" << endl;
    cout << "4. Pomnoz pierwsza przez druga" << endl;
    cout << "5. Podziel pierwsza przez druga" << endl;
    cout << "9. wyjscie z CW2" << endl;
}
void obslugaCW3()
{
    int i = 0;
    Zespolona p1,p2;

    while(i!=9)
    {
        cout << "Wczytaj wartosci wektora [a,b], [c,d]" << endl;
        cin >> p1 >> p2;
        cout << katMiedzyWektorami(p1,p2) << endl;
        cout << "wybierz 9, aby wyjsc z CW3 lub dowolna inna liczbe, aby kontynuowac" << endl;
        cin >> i;
    }
}
int main()
{

    int opcja = 0,opcja2 = 0;
    Zespolona a,b,c;
    while(opcja2 != 9)
    {
        obslugaMenu();
        cin >> opcja2;
        switch (opcja2)
        {
        case 1:
            cout << "podaj 2 pary liczb zespolonych" << endl;
            cin >> a >> b;
            c = a+b;
            cout << c << endl;
            break;
        case 2:

            obslugaCW2();
            cin >> opcja;
            while(opcja!= 9)
            {
                switch (opcja)
                {
                case 1:
                    cout << "Podaj wartosci a,b pierwszej liczby zespolonej" << endl;
                    cin >> a;
                    cout << "Podaj wartosci a,b drugiej liczby zespolonej" << endl;
                    cin >> b;
                    break;
                case 2:
                    c = a+b;
                    cout << endl << c << endl;
                    break;
                case 3:
                    c = a-b;
                    cout << endl << c << endl;
                    break;
                case 4:
                    c= a*b;
                    cout << endl << c << endl;
                    break;
                case 5:
                    if(b.rozneOdZera())
                    {
                        c=a/b;
                        cout << endl << c << endl;
                    }
                    else
                        cout << endl << "dzielisz przez zero" << endl;
                    break;
                case 9:
                    continue;
                default:
                    cout << endl << "podales zly numer sprobuj jeszcze raz" << endl;
                }
                obslugaCW2();
                cin >> opcja;
            }
            break;
        case 3:
            obslugaCW3();
            break;
        case 9:
            continue;
        default:
            cout << "podales zly numer sprobuj jeszcze raz" << endl;
        }
    }
    return 0;
}
