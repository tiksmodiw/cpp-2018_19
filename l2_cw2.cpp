#include <iostream>

using namespace std;


class Waga{
private:
    double wartosc;
    const string jednostka;

public:
    friend string compareObj(Waga *w1, Waga *w2, double wspolczynnik);
    Waga(){}
    Waga(double _wartosc, const string str): jednostka(str){
        wartosc = _wartosc;
    }
    void showMass(){
        cout << wartosc <<" " <<jednostka << "\n";
    }
    void massByHalf(){
        wartosc/=2;
    }

};

string compareObj(Waga *w1, Waga *w2, double wspolczynnik){
string s1,s2,s3;
s1 = "jednostki sa rowne \n";
s2 = "pierwsza jest wieksza\n";
s3 = "druga jest wieksza\n";
    if(w1->wartosc == w2->wartosc*wspolczynnik) return s1;
    else if(w1->wartosc >= w2->wartosc*wspolczynnik) return s2;
    else return s3;
}
int main()
{
    double weight, wspolczynnik;
    string unit;
    Waga *w1 = new Waga(1.0, "KG");
    cout << "Podaj wage obiektu, rodzaj jednostki i wspolczynnik do kilograma\n";
    cin >> weight >> unit >> wspolczynnik;
    Waga *waga = new Waga(weight,unit);
    cout << compareObj(w1,waga, wspolczynnik);

    waga->massByHalf();
    return 0;
}
